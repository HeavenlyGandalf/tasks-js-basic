// Solutions for homework tasks

// Insert Task 1 solution here
var longestCommonPrefix = function (strs) {
  let shortestString = strs.reduce(function (shortest, current) {
    return current.length < shortest.length ? current : shortest;
  }, strs[0]);

  function isCommonPrefix(prefix, strs) {
    return strs.every((str) => str.startsWith(prefix));
  }
  let low = 0;
  let high = shortestString.length;

  while (low < high) {
    let mid = Math.floor((low + high) / 2);
    let prefix = shortestString.slice(0, mid);

    if (isCommonPrefix(prefix, strs)) {
      low = mid + 1;
    } else {
      high = mid;
    }
  }

  if (low > 0 && !isCommonPrefix(shortestString.slice(0, low), strs)) {
    low--;
  }
  return shortestString.slice(0, low);
};

// Insert Task 2 solution here
var countPrimes = function (n) {
  let primes = new Array(n).fill(true);
  primes[0] = primes[1] = false;

  for (let i = 2; i * i < n; i++) {
    if (primes[i]) {
      for (let j = i * i; j < n; j += i) {
        primes[j] = false;
      }
    }
  }

  let count = 0;
  for (let i = 2; i < n; i++) {
    if (primes[i]) {
      count++;
    }
  }

  return count;
};
// Insert Task 3 solution here

var romanToInt = function (s) {
  symbols = {
    I: 1,
    V: 5,
    X: 10,
    L: 50,
    C: 100,
    D: 500,
    M: 1000,
  };

  let result = 0;

  for (let i = 0; i < s.length; i++) {
    const current = symbols[s[i]];
    const next = symbols[s[i + 1]];
    if (next && current < next) {
      result -= current;
    } else {
      result += current;
    }
  }
  return result;
};

// Insert Task 4 solution here

var merge = function (nums1, m, nums2, n) {
  let i = m - 1;
  let j = n - 1;
  let k = m + n - 1;

  while (i >= 0 && j >= 0) {
    if (nums1[i] > nums2[j]) {
      nums1[k] = nums1[i];
      i--;
    } else {
      nums1[k] = nums2[j];
      j--;
    }
    k--;
  }

  while (j >= 0) {
    nums1[k] = nums2[j];
    j--;
    k--;
  }
};

// Insert Task 5 solution here

var Solution = function (nums) {
  this.original = nums.slice();
  this.array = nums.slice();
};

Solution.prototype.reset = function () {
  this.array = this.original.slice();
  return this.array;
};

Solution.prototype.shuffle = function () {
  let rand;
  for (let i = this.array.length - 1; i > 0; i--) {
    rand = Math.floor(Math.random() * (i + 1));
    [this.array[i], this.array[rand]] = [this.array[rand], this.array[i]];
  }
  return this.array;
};
